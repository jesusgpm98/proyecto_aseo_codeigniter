<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once('libwebpay/webpay.php');

class WebPay_lib {

    private $config = null;
    public  $webpay = null;

    function __construct()
    {
        $certificate = require_once('certificates/cert-normal.php');

        $this->config = new configuration();
        $this->config->setEnvironment($certificate['environment']);
        $this->config->setCommerceCode($certificate['commerce_code']);
        $this->config->setPrivateKey($certificate['private_key']);
        $this->config->setPublicCert($certificate['public_cert']);
        $this->config->setWebpayCert($certificate['webpay_cert']);

        $this->webpay = new Webpay($this->config);
    }

    function init_transaction($monto, $orden_compra, $session_id = NULL, $url_return = NULL, $url_final = NULL)
    {
        if(!$session_id) {
            $session_id = mt_rand(0, 30549846);
        }

        if(!$url_return) {
            $url_return = base_url('Datos_pago/webpay_comprobante');
        }

        if(!$url_final) {
            $url_final = base_url('Datos_pago/webpay_error?anulado=true');
        }

        $result = $this->webpay
                        ->getNormalTransaction()
                        ->initTransaction($monto, $orden_compra, $session_id, $url_return, $url_final);



        return $result;
    }

    function get_transaction_result($token)
    {
        $result = $this->webpay->getNormalTransaction()->getTransactionResult($token);
        return $result;
    }


}
