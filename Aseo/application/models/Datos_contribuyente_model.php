  <?php defined('BASEPATH') OR exit('No direct script access allowed');

class Datos_contribuyente_model extends CI_Model{
  function __construct(){
    parent::__construct();
  }

  public function getCliente($rol,$rol_dv){

    $this->db->select('rol, rol_dv, rut, rut_dv, nombre, direccion, email_usuario');
    $this->db->from('aseo');
    $this->db->where("rol='$rol' AND rol_dv='$rol_dv'");
    $this->db->limit('1');
    $query = $this->db->get();
    return $query->result_array();
  }

  public function getCuota($rol,$rol_dv){
    $this->db->select('c.ano, cuota, fec_vcto,valor_cuota, interes_pagado, multa_pagado');
    $this->db->from('cuota c');
     $this->db->join('aseo a', 'a.rol = c.rol AND a.rol_dv = c.rol_dv AND c.ano = a.ano');
    $this->db->where("(c.rol='$rol' AND c.rol_dv='$rol_dv')");
    $this->db->where('c.estado', '0');
    $this->db->order_by('c.ano','ASC');
    $this->db->order_by('c.cuota','ASC');
    $query = $this->db->get();
    return $query->result_array();
  }

  public function getIPC($anoActual,$mesActual,$anioVencimiento){

    $this->db->select('ipc_may, interes_may, ipc_jul, interes_jul, ipc_oct, interes_oct, ipc_dic, interes_dic');
    $this->db->from('parametros_generales.maestro_multas');
    $this->db->where('ano',$anoActual);
    $this->db->where('mes',$mesActual);
    $this->db->where('ano_tabla',$anioVencimiento);
    $query = $this->db->get();
    return $query->result_array();
  }

  public function insertInteres($interes,$multa,$ano,$cuota){

    $data = array('interes_pagado' => $interes,
                  'multa_pagado' => $multa);

    $this->db->set($data);
    $this->db->where_in('ano',$ano);
    $this->db->where('cuota',$cuota);
    $this->db->where('estado', 0);
    $this->db->update('cuota');
  }

  public function updtMultaInteres($rol){

    $data = array('interes' => '0',
                  'multa' => '0');

    $this->db->set($data);
    $this->db->where('rol',$rol);
    $this->db->where('estado','0');
    $this->db->update('cuota');
    }

}
