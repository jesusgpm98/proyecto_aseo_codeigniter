<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Datos_pago_model extends CI_Model{
  function __construct(){
    parent::__construct();
  }

  public function getPagoId(){

    $this->db->select('IFNULL(MAX(pago_id)+1,1) pago_id');
    $this->db->from('web_pago');
    $query = $this->db->get();
    return $query->result_array();
  }

  public function getId(){//para el pdf
    $this->db->select('MAX(pago_id) pago_id');
    $this->db->from('web_pago');
    $query = $this->db->get();
    return $query->result_array();
  }

  public function insertPrePago($pago_id,$total,$datos){

    $date = date('Ymd');
    $data = array('pago_id' => $pago_id,
                  'cuotas' => $datos,
                  'total_cancelado' => $total,
                  'create_date' => $date);

    $this->db->insert('web_pago',$data);
  }

  public function updatePago(transactionResultOutput $webay){

        $fecha_pago         = date('YmdHms');
        $estado             = 'Pagado';
        $tbk_orden_compra   = $webay->buyOrder;
        $tbk_transaccion    = 'TR_NORMAL';
        $tbk_respuesta      = $webay->detailOutput->responseCode;
        $tbk_monto          = $webay->detailOutput->amount;
        $tbk_cod_aut        = $webay->detailOutput->authorizationCode;
        //$tbk_nro_tarjeta    = '';
        $tbk_final_num_tarj = $webay->cardDetail->cardNumber;
        $tbk_fecha_expira   = $webay->cardDetail->cardExpirationDate;
        $tbk_fecha_contable = $webay->accountingDate;
        $tbk_fecha_transacc = $webay->transactionDate;
        //$tbk_hora_transacc  = '';
        $tbk_id_session     = $webay->sessionId;
        //$tbk_id_transacc    = '';
        $tbk_tipo_pago      = $webay->detailOutput->paymentTypeCode;
        $tbk_nro_cuota      = $webay->detailOutput->sharesNumber;
        $sw_tipo_pago       = 'TBK';

        $dato = $this->db->select('IFNULL(MAX(folio)+1,1) folio')
                          ->from('web_pago')
                          ->get()
                          ->first_row('array');

        $data = array(
        'TBK_ORDEN_COMPRA'          => $tbk_orden_compra,
        'TBK_TIPO_TRANSACCION'      => $tbk_transaccion,
        'TBK_RESPUESTA'             => $tbk_respuesta,
        'TBK_MONTO'                 => $tbk_monto,
        'TBK_CODIGO_AUTORIZACION'   => $tbk_cod_aut,
        'TBK_FINAL_NUMERO_TARJETA'  => $tbk_final_num_tarj,
        'TBK_FECHA_EXPIRACION'      => $tbk_fecha_expira,
        'TBK_FECHA_CONTABLE'        => $tbk_fecha_contable,
        'TBK_FECHA_TRANSACCION'     => $tbk_fecha_transacc,
        'TBK_ID_SESION'             => $tbk_id_session,
        'TBK_TIPO_PAGO'             => $tbk_tipo_pago,
        'TBK_NUMERO_CUOTAS'         => $tbk_nro_cuota,
        'folio'                     => $dato['folio'],
        'pago_id'                   => $tbk_orden_compra,
        'total_cancelado'           => $tbk_monto,
        'estado'                    => 'Pagado'
        );

        $this->db->set($data);
        $this->db->where('pago_id',$tbk_orden_compra);
        $this->db->update('web_pago');
  }

  public function getListCuotas($pago_id){

    $this->db->select('cuotas');
    $this->db->from('web_pago');
    $this->db->where('pago_id',$pago_id);
    $query = $this->db->get();
    return $query->result_array();
  }


  public function updatePagoCuota($pago_id,$ano,$rol,$rol_dv,$cuota){

    $data = array('pago_id' => $pago_id,
                  'estado'  => '1');

    $this->db->set($data);
    $this->db->where('ano',$ano);
    $this->db->where('rol',$rol);
    $this->db->where('rol_dv',$rol_dv);
    $this->db->where('cuota', $cuota);
    $this->db->update('cuota');
  }

  public function getDatosPdf($pago_id,$ano,$cuota,$rol,$rol_dv){
    //$this->output->enable_profiler(TRUE);

    $this->db->select('*');
    $this->db->from('aseo a');
    $this->db->join('cuota c','a.ano = c.ano AND a.rol = c.rol AND a.rol_dv = c.rol_dv');
    $this->db->join('web_pago w', 'w.pago_id = c.pago_id');
    $this->db->where('c.pago_id', $pago_id);
    $this->db->where('c.ano', $ano);
    $this->db->where('c.cuota', $cuota);
    $this->db->where('c.rol', $rol);
    $this->db->where('c.rol_dv', $rol_dv);
    $this->db->where('c.estado','1');
    $query = $this->db->get();
    return $query->result_array();
  }

  public function getDatos($pago_id){
    //$this->output->enable_profiler(TRUE);

    $this->db->select('*');
    $this->db->from('aseo a');
    $this->db->join('cuota c','a.ano = c.ano AND a.rol = c.rol AND a.rol_dv = c.rol_dv');
    $this->db->join('web_pago w', 'w.pago_id = c.pago_id');
    $this->db->where('c.pago_id', $pago_id);
    $this->db->where('c.estado','1');
    $query = $this->db->get();
    return $query->result_array();
  }

  public function get_valor_cuota($pago_id,$ano,$cuota,$rol,$rol_dv){
    $this->db->select('SUM(valor_cuota + interes_pagado + multa_pagado) as total');
    $this->db->from('cuota');
    $this->db->where('pago_id', $pago_id);
    $this->db->where('ano', $ano);
    $this->db->where('cuota', $cuota);
    $this->db->where('rol', $rol);
    $this->db->where('rol_dv', $rol_dv);
    $this->db->where('estado','1');
    $query = $this->db->get();
    return $query->result_array();
  }

  public function get_roles($pago_id,$ano,$cuota){
    //$this->output->enable_profiler(TRUE);
    $this->db->select('c.rol, c.rol_dv');
    $this->db->from('cuota c');
    $this->db->join('web_pago w', 'w.pago_id = c.pago_id');
    $this->db->where('w.pago_id',$pago_id);
    $this->db->where('c.ano', $ano);
    $this->db->where('c.cuota', $cuota);

    $query = $this->db->get();
    return $query->result_array();
  }

  public function getFolioPdf($pago_id,$ano,$cuota,$rol,$rol_dv){
    $this->db->select('folio as folioFinal');
    $this->db->from('cuota');
    $this->db->where('pago_id', $pago_id);
    $this->db->where('ano', $ano);
    $this->db->where('cuota', $cuota);
    $this->db->where('rol', $rol);
    $this->db->where('rol_dv', $rol_dv);
    $this->db->where('estado','1');
    $query = $this->db->get();
    return $query->result_array();
  }


  public function updateFirma($pdf,$ano,$cuota,$rol,$rol_dv){

    $data = array('doc_descar' => $pdf);

    $this->db->set($data);
    $this->db->where('ano',$ano);
    $this->db->where('cuota',$cuota);
    $this->db->where('rol',$rol);
    $this->db->where('rol_dv',$rol_dv);
    $this->db->update('cuota');
  }

  public function getPago($rol, $rol_dv){
  //  $this->output->enable_profiler(TRUE);
  $this->db->select('CONCAT(rol,"-",rol_dv) as rol,ano,cuota,pago_id,doc_descar');
  $this->db->from('cuota');
  $this->db->where('rol',$rol);
  $this->db->where('rol_dv',$rol_dv);
  $this->db->where('pago_id <>','0');

  $query = $this->db->get();
  return $query->result_array();
}

  public function get_datos($pago_id){

  $this->db->select('cuotas');
  $this->db->from('web_pago');
  $this->db->where('pago_id',$pago_id);

  $query = $this->db->get();
  return $query->result_array();
}




}
