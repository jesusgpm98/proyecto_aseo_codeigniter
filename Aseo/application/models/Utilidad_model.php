<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Utilidad_model extends CI_Model{
  function __construct(){
    parent::__construct();
  }

  public function send_mailcomprobante_pago($pago_id,$ano,$cuota,$rol,$rol_dv){

    $web_pago   = $this->db
                            ->select('create_date,
                                    total_cancelado,
                                    TBK_NUMERO_CUOTAS,
                                    TBK_TIPO_PAGO,
                                    TBK_CODIGO_AUTORIZACION,
                                    TBK_ORDEN_COMPRA,
                                    pago_id')
                            ->from('web_pago')
                            ->where([
                                'pago_id'   => $pago_id
                            ])
                            ->get()
                            ->first_row('array');

        if(empty($web_pago)) {
            return null;
        }

        $info   = $this->db
                            ->select('nombre,
                                    rut,
                                    rut_dv,
                                    c.rol,
                                    c.rol_dv,
                                    email_usuario,
                                    doc_descar')
                            ->from('aseo a')
                            ->join('cuota c', 'a.rol = c.rol AND a.rol_dv = c.rol_dv')
                            ->where([
                                'pago_id'   => $pago_id,
                                'c.ano'     => $ano,
                                'c.cuota'   => $cuota,
                                'c.rol'     => $rol,
                                'c.rol_dv'  => $rol_dv
                            ])
                            ->get()
                            ->first_row('array');

            if(empty($info)) {
                return null;
            }

            $data = [
              'wp'    => $web_pago,
              'info'  => $info
          ];

        $email_to   = $info['email_usuario'];

        $this->load->library('email');
        $this->email
            ->from('Patentes@insico.cl', 'Comprobante de Pago')
            ->to($email_to)
            ->subject('Pago de Aseo')
            ->message($this->load->view('email/template', $data, TRUE))
            ->set_mailtype('html');

        $this->email->send();

        //$this->load->view('email/template', $data);
        return true;
  }

  public function update_email($new_email, $rol, $rol_dv){
    $this->db->set('email_usuario', $new_email);
    $this->db->where('rol', $rol);
    $this->db->where('rol_dv', $rol_dv);
    $this->db->update('aseo');
  }
}
