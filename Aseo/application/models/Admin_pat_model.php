<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_pat_model extends CI_Model{
  function __construct(){
    parent::__construct();
  }

  public function login_user($user,$pass) {

  $this->db->select('*');
  $this->db->from('user_info');
  $this->db->join('auth_user_unit','user_info.user_id = auth_user_unit.user_id');
  $this->db->where('username',$user);
  $this->db->where('password',$pass);
  $query = $this->db->get();
  if($query->num_rows() == 1)
    {
      return $query->row();
    }else{
      redirect(base_url('Admin_Pat/login?error=true'));
    }
}

function get_listado_comprobantes($desde, $hasta, $page = 1, $limit = 10)
    {
    //  $this->output->enable_profiler(TRUE);
        $desde  = str_replace("/", "", $desde);
        $hasta  = str_replace("/", "", $hasta);
        $offset     = (($page-1) * $limit);

        $result = $this->db
                  ->select('0 AS numero,
                         w.create_date,
                         CONCAT(c.rol,"-",c.rol_dv) as rol,
                         CONCAT(c.cuota,"_",c.ano) as cuota,
                         c.folio,
                         CONCAT(a.rut,"-",a.rut_dv) as rut,
                         a.nombre as nombre,
                         a.direccion as direccion,
                         c.valor_cuota,
                         c.interes_pagado,
                         c.multa_pagado,
                         (c.valor_cuota + c.interes_pagado + c.multa_pagado) as total_cuota,
                         w.total_cancelado,
                         w.create_date,
                         w.total_cancelado,
                         w.TBK_CODIGO_AUTORIZACION,
                         w.TBK_FINAL_NUMERO_TARJETA,
                         w.pago_id,
                         w.TBK_TIPO_TRANSACCION')
                  ->from('aseo a')
                  ->join('cuota c', 'a.rol = c.rol AND a.rol_dv = c.rol_dv AND a.ano = c.ano')
                  ->join('web_pago w', 'w.pago_id = c.pago_id')
                  ->where([
                    'c.estado'                     => 1,
                    'LEFT(w.create_date, 8) >='    => $desde,
                    'LEFT(w.create_date, 8) <='    => $hasta,
                    'w.estado'                     => 'Pagado'
                ]);

        $result = $result
                        ->order_by('w.pago_id')
                        ->get()
                        ->result_array();
       if(empty($result)) {
           return null;
       }

       $size = count($result);
       for($i = 0; $i < $size; $i++) {
           $result[$i]['numero'] = ($i+1) + $offset;
       }

       $resume= $this->db
                     ->select('COUNT(*) AS pagos,
                                SUM(c.valor_cuota + c.interes_pagado + c.multa_pagado) as total_cancelado')
                     ->from('cuota c')
                     ->join('web_pago wp', 'c.pago_id = wp.pago_id')
                     ->where([
                            'c.estado'                  => 1,
                            'LEFT(create_date, 8) >='    => $desde,
                            'LEFT(create_date, 8) <='    => $hasta,
                            'wp.estado'                 => 'Pagado'
                        ])
                        ->get()
                        ->first_row('array');

        if(empty($resume)) {
           return null;
        }
        return [
           'data'       => $result,
           'resume'     => $resume
        ];
    }
}
