<?php
defined('BASEPATH') OR exit('No direct script access allowed');

  if(!function_exists('cal_dias')){

      function cal_dias($fecha){

        $dia = substr($fecha,6,2);
        $mes = substr($fecha,4,2);
        $ano = substr($fecha,0,4);

                               //h.m.s.mes.dia.año
        $fechaInicial   = mktime(0,0,0,$mes ,$dia ,$ano);
    		$lapso          = 5; //  dias habiles
    		$diasTrans      = 0; // dias transcurridos
    		$diasHabiles    = 0;
    		$feriados       = array();

        while($diasHabiles<($lapso+1)){

          $fecha = $fechaInicial+($diasTrans*86400);
          $diaSemana  = getdate($fecha);
            /* Para calcular solo días habiles
            if($diaSemana["wday"]!=0 && $diaSemana["wday"]!=6){

              $feriado    = $diaSemana['mday']."-".$diaSemana['mon'];
                if(!in_array($feriado,$feriados)){

                  $diasHabiles++;
                }
            }*/
            $diasHabiles++;
			      $diasTrans++;
        }
        $fechaFinal     = $fechaInicial+(($diasTrans-1)*86400);
        $fechaFinDesc = date("dmY",$fechaFinal);

        return $fechaFinDesc;
      }
  }

  if(!function_exists('cal_monto')){

    function cal_monto($fecha1,$gravedad,$utm,$montoUtm){

        $fecha2= date('dmY');
        if(($fecha1 - $fecha2)>=0 && $gravedad=='leve'){

          $neto = round((0.2) * $utm);
          $montocal = round((0.2) * $utm);
          $montoreb = round((0.2) * $utm);

          $descuento = round(($montocal * 25) / 100);
        }elseif(($fecha1 - $fecha2)>=0 && $gravedad=='Menos Grave'){

          $neto = round((0.5) * $utm);
          $montocal = round((0.5) * $utm);
          $montoreb = round((0.5) * $utm);

          $descuento = round(($montocal * 25) / 100);
        }elseif(($fecha1 - $fecha2)>=0 && $gravedad=='Grave'){

          $neto = round((1) * $utm);
          $montocal = round((1) * $utm);
          $montoreb = round((1) * $utm);

          $descuento = round(($montocal * 25) / 100);
        }else{

          $neto = round(($montoUtm) * $utm);
          $montocal = round(($montoUtm) * $utm);
          $montoreb = round(($montoUtm) * $utm);

          $descuento = 0;
        }

        return $neto."_".$montocal."_".$montoreb."_".$descuento;
    }
  }

  if(!function_exists('formatDate')){

    function formatDate($fecha, $fmt="d/m/Y"){

      $tmp = ymd2time($fecha);
		  return date($fmt, $tmp);
    }
  }

  if(!function_exists('ymd2time')){

    function ymd2time($fecha){

      $ano = substr($fecha, 0, 4);
		  $mes = substr($fecha, 4, 2);
	 	  $dia = substr($fecha, 6, 2);

		  return mktime(0, 0, 0, $mes, $dia, $ano);
    }
  }

  if(!function_exists('formatDate2')){

    function formatDate2($fecha, $fmt="d/m/Y"){

      $tmp = ymd2time2($fecha);
		  return date($fmt, $tmp);
    }
  }

  if(!function_exists('ymd2time2')){

    function ymd2time2($fecha){

      $ano = substr($fecha, 4, 4);
		  $mes = substr($fecha, 2, 2);
		  $dia = substr($fecha, 0, 2);

		  return mktime(0, 0, 0, $mes, $dia, $ano);
    }
  }

  if(!function_exists('formatPesos')) {

    function formatPesos($mto=0, $dec=0) {
      return "$" . number_format($mto, $dec, ",", ".");
    }
}

if (!function_exists('format_payment_code'))
{
  function format_payment_code($code)
  {
      switch ($code) {
          case 'VD': return 'Venta Debito';
          case 'VN': return 'Venta Normal';
          case 'SI': return '3 cuotas sin interés';
          case 'S2': return '2 cuotas sin interés';
          case 'NC': return 'N cuotas sin interés';
          case 'VC': return 'Venta en cuotas';
          default: return 'Sin Información';
      }
  }
}

if(!function_exists('format_pesos')) {

    function format_pesos($mto=0, $dec=0) {
      return "$" . number_format($mto, $dec, ",", ".");
    }
}

if(!function_exists('send_file_to_ftp'))
{
    function send_file_to_ftp($data, $name_file)
    {
        $ci =& get_instance();
        $ci->load->library('ftp');


        $ftpConfig      = $ci->config->item('ftp');
        $ftpDir         = get_path_certificados('FTP');


        $tmp_file   = tempnam(sys_get_temp_dir(), 'permiso_');
        $tmp_file   = $tmp_file . "_" . $name_file;
        $uploaded   = true;


        // Crear el fichero temporal
        file_put_contents($tmp_file, $data);

        if($ci->ftp->connect($ftpConfig)) { // Conexion Correcta

            // Crear el directorio si es necesario
            $ci->ftp->mkdir($ftpDir);

            // Se sube el fichero
            if(!$ci->ftp->upload($tmp_file, "$ftpDir$name_file")) {
                $uploaded = false;
            }

            // Cierre de conexion
            $ci->ftp->close();

        } else { // Conexion Erronea
            $uploaded = false;
        }

        if($uploaded == false) { // El archivo no se subio al FTP, Se guarda de forma local

            $localDir   = get_path_certificados('LOCAL');

            // Crear el directorio si no existe
            if(!is_dir($localDir)) {
                mkdir($localDir, 0777, true);
                //echo "<h3>Solución: Asignar permisos (0777) a la carpeta donde se guardan los certificados</h3>";
            }

            $finalPath  ="$localDir$name_file";
            rename ($tmp_file, $finalPath);
        }

        // Se elmina el archivo temporal
        @unlink($tmp_file);

        return $uploaded; // TRUE = Se subio al ftp | FALSE = Se guardo de manera local
    }
}

if(!function_exists('get_path_certificados'))
{
    function get_path_certificados($mode = 'FTP')
    {
        $anioFolder     = date('Y');
        if($mode == 'FTP' || $mode === TRUE) {
            $ci =& get_instance();
            return $ci->config->item('ftp')['dir'] . "Firma/$anioFolder/";
        } else {
            return FCPATH . "Firma/$anioFolder/";
        }
    }
}

if (!function_exists('tipo_certificado'))
{
  function tipo_certificado($code)
  {
      switch ($code) {
          case '1': return 'Certificado de Número';
          case '2': return 'Certificado Declaratoria de Utilidad Pública';
          case '3': return 'Certificado de Uso de Suelo';
          case '4': return 'Certificado de Informaciones Previas';
          default: return 'Sin Información';
      }
  }
}

if (!function_exists('getTipo'))
{
  function getTipo($code)
  {
      switch ($code) {
          case '1': return 'Cert.Nro.';
          case '2': return 'Cert.UPub.';
          case '3': return 'Cert.Suelo';
          case '4': return 'Cert.IPrevias';
          default: return 'Sin Información';
      }
  }
}

if(!function_exists('is_valid_date'))
{
    function is_valid_date($date, $format = 'Y/m/d')
    {
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }
}

if ( ! function_exists('redirect_back'))
{
    function redirect_back()
    {
        if(isset($_SERVER['HTTP_REFERER']))
        {
            header('Location: '.$_SERVER['HTTP_REFERER']);
        }
        else
        {
            header('Location: http://'.$_SERVER['SERVER_NAME']);
        }
        exit;
    }
}
