<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inicio extends CI_Controller {
	function __construct(){
    parent::__construct();
		$this->load->helper('sistema_helper');
		$this->load->library('session');
	}

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->template('index/inicio');
	}

	public function reimprimir(){

		$this->load->template('index/reimprime');
	}

	public function reimpresion(){

		$rol = $this->input->post('Txtrol');
		$rol_dv = $this->input->post('txtRolDv');

		//var_dump($rol,$rol_dv);
		$this->load->model('Datos_pago_model');

		$pago = $this->Datos_pago_model->getPago($rol,$rol_dv);
		//var_dump($pago);
		$data = array('pago' => $pago);

		$this->load->template('index/reimpresion',$data);

	}
}
