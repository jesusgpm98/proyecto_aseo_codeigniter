<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Datos_pago extends CI_Controller {
  function __construct(){
    parent::__construct();
		$this->load->helper('sistema_helper');
    $this->load->model('datos_pago_model');
		$this->load->model('utilidad_model');
    $this->load->library('session');
	}

  public function Confirmacion(){

    $rol = $this->input->post('txtRol');
    $rol_dv = $this->input->post('txtRolDv');
    $total = $this->input->post('otro_total');
    $data['txtTotal'] = $total;
    if(!$total){
      $total = $this->input->post('txtTotal');
      $data['txtTotal'] = $total;
    }//else{


      $datos = $this->input->post('check_aseo');

      if(!$datos){
        $this->load->template('errors/system/error');
    }else{
      $list_pagos = implode(',',$datos);
      $data['datos'] = $datos;


      try {
        $pago_id = $this->datos_pago_model->getPagoId();
      } catch (Exception $e) {
        throw new Exception("No se puede obtener el PagoID");
      }

      try {
        $web_pago = $this->datos_pago_model->insertPrePago($pago_id[0]['pago_id'],$total,$list_pagos);
      } catch (Exception $e) {
        throw new Exception("Error al insertar PrePago".$e);
      }

      $this->load->library('webpay_lib');
      $orden_compra = $pago_id[0]['pago_id'];
      $result = $this->webpay_lib->init_transaction($total, $orden_compra);
      $data ['webpay'] = $result;

      $this->load->library('session');
      $this->session->set_userdata('transaccion', TRUE); // Session activa

      $this->load->template('pago/datos_pago',$data);
    }
  }

  public function webpay_error(){

    $this->load->library('session');
        if(!$this->session->userdata('transaccion')) {
            redirect('/');
        }
    $orden_compra   = $this->input->get('id', TRUE);
    $reload         = $this->input->get('reload', TRUE);
    $anulado        = $this->input->get('anulado', TRUE);

    $data = [
            'pago_id'   => $orden_compra,
            'reload'    => $reload,
            'anulado'   => $anulado
        ];

    $this->load->template('pago/webpay_error',$data);
  }

  public function webpay_comprobante(){

    //$this->output->enable_profiler(TRUE);
    $this->load->library('session');
			if(!$this->session->userdata('transaccion')) {
					redirect('/');
			}
			error_reporting(E_ALL ^ E_DEPRECATED); // Desactivar advertencias deprecated de soap/xml
    $token  = $this->input->post('token_ws');
    if(empty($token)) {
      throw new Exception("Error no se a retornado el token desde webpay");
    }
    $this->load->library('webpay_lib');
	  $result = $this->webpay_lib->get_transaction_result($token);

    if(!is_array($result)) {
      if($result->detailOutput->responseCode == 0){

        $pago_id = $result->detailOutput->buyOrder;
        $ruta_pdf="";

        $this->datos_pago_model->updatePago($result);
        $roles = $this->datos_pago_model->getListCuotas($pago_id);

        $datos_rol = explode(',',$roles[0]['cuotas']);

        foreach($datos_rol as $row):
          $datos = explode('_',$row);
         $ano   = $datos[0];
         $total = $datos[1];
         $cuota = $datos[2];
         $asd   = $datos[3];

        $split = explode('-',$datos[3]);

         $rol = $split[0];
         $rol_dv = $split[1];
         //var_dump($split,$rol,$rol_dv);

          $this->datos_pago_model->updatePagoCuota($pago_id,$ano,$rol,$rol_dv,$cuota);
          $pdf = $this->Firmadoc($pago_id,$ano,$cuota,$rol,$rol_dv);
          //var_dump($pdf);
          $datos_pdf = $this->firmar_pdf_ecert($pdf);
          //var_dump($datos_pdf);
           if($datos_pdf !== null) {

					 					 $cert_name              = $datos_pdf['file'];
					 					 $cert_guid              = 'http://plataformafirma.ecertchile.cl/SitioWeb/Consultas/VerDocPDF/'.$datos_pdf['guid'];
					 					 $cert_contenido         = &$datos_pdf['pdf'];

					 					 //$ftp = send_file_to_ftp($cert_contenido, $cert_name);
					 					 $ruta_pdf =  (get_path_certificados('ECERT') . $cert_guid);
					 				   $asd = $this->datos_pago_model->updateFirma($cert_guid,$ano,$cuota,$rol,$rol_dv);
                     $this->utilidad_model->send_mailcomprobante_pago($pago_id,$ano,$cuota,$rol,$rol_dv);
                      //var_dump($asd);
					 					 //echo 'firma realizada';

					 			 } else {
										 log_message('error', "[Pago->webpay_comprobante()]: No se pudo firmar el documento del pago_id $pago_id.");
										 //echo 'error firma';
								 }
        endforeach;

        $data = [
                   'webpay' => $result,
                   'ruta_pdf'=> $cert_guid
               ];
        $this->load->template('pago/webpay_comprobante', $data);
      }else{
        $buyOrder = $result->detailOutput->buyOrder;
        redirect("Datos_pago/webpay_error?id=$buyOrder");
      }
    }else{
      redirect("Datos_pago/webpay_error?reload=true");
    }
  }

   public function Firmadoc($pago_id,$ano,$cuota,$rol,$rol_dv){
    // $this->output->enable_profiler(true);

    $datos = $this->datos_pago_model->getDatosPdf($pago_id,$ano,$cuota,$rol,$rol_dv);
    //var_dump($datos);
    $folio = $this->datos_pago_model->getFolioPdf($pago_id,$ano,$cuota,$rol,$rol_dv);
    $valorCuota = $this->datos_pago_model->get_valor_cuota($pago_id,$ano,$cuota,$rol,$rol_dv);



    $this->load->library('myfpdi');
	 	$this->load->library('myfpdf');

    $pdf = new setasign\Fpdi\Fpdi('P','mm','Letter');
    $pdf->AddPage();
    $pdf->Image(base_url('resources/img/comprobante.png'),0,0,216,120);
    $pdf->Image(base_url('resources/img/Timbre.png'),100,68,22,22);

    //cabecera
    $pdf->setFont('Arial','B',12);
    $pdf->SetXY(160,14);
    $pdf->cell(55,5, 'Fecha:');
    $pdf->SetXY(176,14);
    $pdf->cell(55,5, date('d/m/Y'));

    $pdf->SetXY(66,14);
    $pdf->cell(55,5, 'Folio:');
    $pdf->SetXY(80, 14);
    $pdf->Cell(24,5,$folio[0]['folioFinal']); //$final[folio]
    //fin cabecera

    $pdf->SetFont('Arial','B',10);
    $pdf->SetXY(4,28);
    $pdf->Cell(50,3,utf8_decode('Nombre o Razón Social:'));
    $pdf->SetXY(46,28);
    $pdf->Cell(15,3,$datos[0]['nombre']);

    $pdf->SetFont('Arial','B',12);
    $pdf->SetXY(4,38);
    $pdf->Cell(50,3,utf8_decode('Dirección:'));
    $pdf->SetXY(26, 37);
    $pdf->Cell(35,5,utf8_decode($datos[0]['direccion']));

    $pdf->SetFont('Arial','B',12);
    $pdf->SetXY(134, 28);
    $pdf->Cell(35,5,'Rol: ');
    $pdf->SetXY(144, 28);
    $pdf->Cell(20,5,$datos[0]['rol'].'-'.$datos[0]['rol_dv']);

    $pdf->SetXY(134, 37);
    $pdf->Cell(35,5,'RUT: ');
    $pdf->SetXY(145, 37);
    $pdf->Cell(20,5,$datos[0]['rut'].'-'.$datos[0]['rut_dv']);
    //fin-detalle usuario

    //detalle boleta
    $pdf->SetFont('Arial','B',12);
    $pdf->SetXY(36, 51);
    $pdf->Cell(35,5,'DETALLE COMPROBANTE');

    $pdf->SetXY(7, 53);
    $pdf->Cell(35,5,'________________________________________________');

    $pdf->SetXY(5, 62);
    $pdf->Cell(35,5,'Estado: ');
    $pdf->SetXY(23, 62);
    $pdf->Cell(16,5, $datos[0]['estado']); //$final[estado]

    $pdf->SetXY(5, 70);
    $pdf->Cell(35,5,utf8_decode('Año: '));
    $pdf->SetXY(17, 70);
    $pdf->Cell(15,5, $datos[0]['ano']); //$final[año]

    $pdf->SetXY(5, 78);
    $pdf->Cell(35,5,'Fecha Vencimiento: ');
    $pdf->SetXY(47, 78);
    $pdf->Cell(20,5,date('d/m/Y', strtotime($datos[0]['fec_vcto']))); //$final[fec_vec]

    $pdf->SetXY(5, 86);
    $pdf->Cell(35,5,'Fecha Pago: ');
    $pdf->SetXY(32, 86);
    $pdf->Cell(24,5, date('d/m/Y', strtotime($datos[0]['create_date']))); //$final[fecha_pago]

    $pdf->SetXY(5, 94);
    $pdf->Cell(35,5,'Cuota: ');
    $pdf->SetXY(20, 94);
    $pdf->Cell(17,5, $datos[0]['cuota'] . ' de 4'); //$final[cuota]

    $pdf->SetXY(153, 51);
    $pdf->Cell(35,5,'DETALLE PAGO');

    $pdf->SetXY(132, 53);
    $pdf->Cell(35,5,'______________________________');

    $pdf->SetXY(130, 62);
    $pdf->Cell(35,5,'Valor Cuota: ');
    $pdf->SetXY(157, 62);
    $pdf->Cell(24,5,formatPesos($datos[0]['valor_cuota'])); //$final[vcuota]

    $pdf->SetXY(130, 70);
    $pdf->Cell(35,5,utf8_decode('Interés Pagado: '));
    $pdf->SetXY(163, 70);
    $pdf->Cell(24,5,formatPesos($datos[0]['interes_pagado'])); //$final[interes]

    $pdf->SetXY(130, 78);
    $pdf->Cell(35,5,'Multa Pagado: ');
    $pdf->SetXY(160, 78);
    $pdf->Cell(24,5,formatPesos($datos[0]['multa_pagado'])); //$final[multa_p]

    $pdf->SetXY(130, 86);
    $pdf->Cell(35,5,'Total Pagado: ');
    $pdf->SetXY(160, 86);
    $pdf->Cell(24,5,formatPesos($valorCuota[0]['total']));
    //fin-detalle boleta

    //detalle transbank
    $pdf->SetXY(130, 94);
    $pdf->Cell(35,5,utf8_decode('Código Transacción: '));
    $pdf->SetXY(174, 94);
    $pdf->Cell(24,5,$datos[0]['TBK_ORDEN_COMPRA']); //$final[TBK_ORDEN_COMPRA]

    $pdf->SetXY(130, 102);
    $pdf->Cell(35,5,utf8_decode('Fecha TransBank: '));
    $pdf->SetXY(168, 102);
    $pdf->Cell(24,5,date('d/m/Y', strtotime($datos[0]['create_date'])));

   return $pdf->Output('S');
 }

  public function firmar_pdf_ecert($raw_data){

        $rut            = "7597394-2" ;// Rut firmante
        $base_64_pdf    = base64_encode($raw_data);
				//var_dump($base_64_pdf);
        $codigo         = "Bin_mmontero";

        $sobre_xml =
        "<Sobre>
            <Cabecera>
                <Firmantes>
                    <DatosFirmante>
                        <Rut>$rut</Rut>
                    </DatosFirmante>
                </Firmantes>
            </Cabecera>
            <Documentos>
                <DatosDocumento tipo=\"PDF\" Codigo=\"$codigo\">
                    <Contenido>
                        $base_64_pdf
                    </Contenido>
                </DatosDocumento>
            </Documentos>
         </Sobre>";
        //var_dump($sobre_xml);

        $soapclient = new SoapClient('http://plataformafirma.ecertchile.cl/WebService/FirmaElectronica.asmx?WSDL');

        $params = array(
            'Usuario'       => 'insico',
            'Clave'         => 'insi2015',
            'SobreXML'      => $sobre_xml
        );

        $response = $soapclient->FirmaDocumento($params);


        $xml = simplexml_load_string($response->FirmaDocumentoResult);
        //var_dump($xml);

        if($xml->Cabecera->Resultado->Estado == 'true') {
            if($xml->Documentos->DatosDocumento->Estado == true) {
                $pdf_firmado    = $xml->Documentos->DatosDocumento->Contenido;
                $guid           = $xml->Documentos->DatosDocumento->Guid;
                //file_put_contents(get_path_certificados('LOCAL') . "firmado.pdf", $pdf_firmado);
                return [
                    'pdf'   => base64_decode($pdf_firmado),
                    'guid'  => $guid,
                    'file'  => "$guid.pdf"
                ];
            }
        }
        return null;
  }


  public function reimpresion(){

    $this->load->model('Datos_pago_model');

    $pago_id = $this->input->get('pago_id');
     $ano = $this->input->get('ano');
     $cuota = $this->input->get('cuota');
     $rol = $this->input->get('rol');
     $rol_dv = $this->input->get('rol_dv');
      //var_dump($pago_id,$ano,$cuota,$rol,$rol_dv);
      $datos = $this->datos_pago_model->getDatosPdf($pago_id,$ano,$cuota,$rol,$rol_dv);
    $pdf = $this->Firmadoc($pago_id,$ano,$cuota,$rol,$rol_dv);

    $datos_pdf = $this->firmar_pdf_ecert($pdf);
    //var_dump($datosPatente);
          //var_dump($datos_pdf);
          if($datos_pdf !== null) {

                     $cert_name              = $datos_pdf['file'];
                     $cert_guid              = 'http://plataformafirma.ecertchile.cl/SitioWeb/Consultas/VerDocPDF/'.$datos_pdf['guid'];
                     $cert_contenido         = &$datos_pdf['pdf'];

                     //$ftp = send_file_to_ftp($cert_contenido, $cert_name);
                     //$ruta_pdf =  (get_path_certificados('ECERT') . $cert_guid);
                     $asd = $this->datos_pago_model->updateFirma($cert_guid,$ano,$cuota,$rol,$rol_dv);
                     $this->utilidad_model->send_mailcomprobante_pago($pago_id);
                     //var_dump($asd);
                     //echo 'firma realizada';

                 } else {
                     log_message('error', "[Pago->webpay_comprobante()]: No se pudo firmar el documento del pago_id $pago_id.");
                     //echo 'error firma';
                 }

  }


}
