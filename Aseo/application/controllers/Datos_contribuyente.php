<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Datos_contribuyente extends CI_Controller {
  function __construct(){
    parent::__construct();
		$this->load->helper('sistema_helper');
		$this->load->model('datos_contribuyente_model');
		$this->load->library('session');
    $this->load->model('utilidad_model');
	}

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function Informacion(){
    //$this->output->enable_profiler(TRUE);
    $rol = $this->input->post('txtRol');
    $rol_dv = $this->input->post('txtRolDv');
    //$direccion = $this->input->post('txtDireccion');
    $correo = $this->input->post('txtCorreo');
    //var_dump($correo);

    $data['correo'] = $correo;

    $cliente = $this->datos_contribuyente_model->getCliente($rol,$rol_dv);
    $data['cliente'] = $cliente;
    //var_dump($cliente);
    $pagos = $this->datos_contribuyente_model->getCuota($rol,$rol_dv);
    //$insertCliente = $this->datos_contribuyente_model->insertarCliente($rol, $rol_dv, $direccion, $correo);

    foreach($pagos as $row):

       $total = $row['valor_cuota'];
       $interes = $this->getMultaInteres($total, $row['fec_vcto'], $row['ano']);
    //var_dump($interes);
    $insertInteres = $this->datos_contribuyente_model->insertInteres($interes['interes'],$interes['multa'],$row['ano'],$row['cuota']);
    endforeach;

    $pagos = $this->datos_contribuyente_model->getCuota($rol,$rol_dv);
    $data['pagos'] = $pagos;

    $this->utilidad_model->update_email($correo, $rol, $rol_dv);

		$this->load->template('datos/datos_contribuyente',$data);
	}

  public function getMultaInteres($total,$fecha_vencimiento,$ano){

     //$mes = substr($fecha_vencimiento, -4,-2);
     $anoVencimiento = substr($fecha_vencimiento, 0,4);
     $mesVencimiento = substr($fecha_vencimiento, 4,-2);
     //var_dump($anoVencimiento);
     //var_dump($mesVencimiento);
     //var_dump($mes);

     // if($mes==04){
     //   $termino = $ano.'0430';
     // }elseif($mes == 06){
     //   $termino = $ano . '0630';
     // }elseif($mes == '09'){
     //   $termino = $ano . '0930';
     // }elseif($mes == 12){
     //   $termino = $ano . '1230';
     // }


     $mesActual = date('m');
     $anioActual = date('Y');
    //
     $ipc = $this->datos_contribuyente_model->getIPC($anioActual,$mesActual,$anoVencimiento);
     //var_dump($ipc);

     //if(strcmp(date('Ymd'),$termino)):
       if($mesVencimiento == 05):
         $factor_ipc = $ipc[0]['ipc_may']/100;
         $factor_multa = $ipc[0]['interes_may']/100;
       elseif($mesVencimiento == 07):
         $factor_ipc = $ipc[0]['ipc_jul']/100;
         $factor_multa = $ipc[0]['interes_jul']/100;
         elseif($mesVencimiento == 10):
           $factor_ipc = $ipc[0]['ipc_oct']/100;
           $factor_multa = $ipc[0]['interes_oct']/100;
           elseif($mesVencimiento == 12):
             $factor_ipc = $ipc[0]['ipc_dic']/100;
             $factor_multa = $ipc[0]['interes_dic']/100;
           else:

       endif;
     //endif;
     $interes = round($total * $factor_ipc);
     //var_dump($interes);
     $multa = round(($total + $total * $factor_ipc) * $factor_multa);
     //var_dump($multa);

     return $multas = array('interes' => $interes,
                             'multa' => $multa);;
  }
}
