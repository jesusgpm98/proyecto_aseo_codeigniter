<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header('Content-Type: text/html; charset=UTF-8');

class Admin_Pat extends CI_Controller {
	function __construct(){
    parent::__construct();
		$this->load->helper('sistema_helper');
		$this->load->library('session');
    $this->load->model('utilidad_model');
		$this->load->model('admin_pat_model');
	}

  public function index(){

		$user = $this->input->post('user');
		$pass = md5($this->input->post('pass'));
		$check_user = $this->admin_pat_model->login_user($user,$pass);

		if($check_user == TRUE){
			$data = array(
						'is_logued_in' => TRUE,
						'nombre'	=> $check_user->user_name,
						'app'			=> $check_user->user_app,
						'perfil' => $check_user->perms,
						'id'			=> $check_user->user_id
				);
				$this->session->set_userdata($data);
				$this->Admin();
			}else{
				redirect(base_url('Admin_Pat/login?error=true'));
			}

	}

	public function herramientas()
 {
		 $data_admin = $this->session->userdata('admin');
		 if(!$data_admin) {
				 $this->session->unset_userdata('admin');
				 redirect('/administracion');
		 }



		 //$default = 'listados';
		 $permisos = [           //WebMaster     OperadorMaster      Operador
				 'listados'      =>  [1,             2,                  3],

		 ];

		 $seccion        = $this->input->get('section');
		 $herramientas   = [];
     //
		 // foreach($permisos as $permiso => $lista) {
			// 	 if(in_array($perfil_admin, $lista)) {
			// 			 $herramientas[$permiso] = TRUE;
			// 	 }
		 // }


		 if(!$seccion) {
				 $seccion = 'listados';
		 } else {
				 if(!array_key_exists($seccion, $herramientas)) {
						 $seccion = 'listados';

				 } else {
						 foreach($herramientas as $permiso => $bool) {
								 if($permiso === $seccion && $bool === TRUE) {
										 $seccion = $permiso;
										 break;
								 }
						 }
				 }
		 }

		 //var_dump($seccion);
		 // cargar desde la vista.
		 $contenido_seccion = $this->herramienta_listados($seccion);



		 $contenido_seccion = $this->{"herramienta_$seccion"}($seccion);

		 //var_dump($perfil_admin);
		 $data = [
				 'contenido'     => $contenido_seccion,
				 'herramientas'  => $herramientas,
				 'nombre_admin'  => $nombre
		 ];

		 $this->load->template('admin-Pat/procesos', $data);
 }


  public function login(){

		$error = $this->input->get('error');
		$this->load->template('Admin-Pat/login',['error' => $error]);
	}

	public function Admin(){

    $nombre = $this->session->userdata('nombre').' '.$this->session->userdata('app');
    $seccion = $this->input->get('section');

    if(!$seccion) {
            $seccion = 'listados';
        }
        //var_dump($seccion);
    $contenido_seccion = $this->{"Admin_Pat_$seccion"}($seccion);


    $data = [
            'contenido'     => $contenido_seccion,
            //'herramientas'  => $herramientas,
            'nombre_admin'  => $nombre
        ];

    $this->load->template('Admin-Pat/procesos',$data);
  }

	public function Admin_Pat_listados($seccion){

    return $this->load->view("Admin-Pat/admin/$seccion", NULL, TRUE);
  }

	public function Admin_Pat_listados_fin(){ //
		//$this->output->enable_profiler(true);
		if(!$this->session->userdata('is_logued_in')){
			redirect('/');
		}

		$desde   = $this->input->post('desde',   TRUE);
		$hasta   = $this->input->post('hasta',   TRUE);
		$listado = $this->input->post('listado', TRUE);

        $lista      = NULL;
        $resumen    = NULL;

				if($desde != null && $hasta != null && $listado != null) {
            if(is_valid_date($desde) && is_valid_date($hasta) && is_numeric($listado)) {

								if($listado==1){
									$result = $this->admin_pat_model->get_listado_comprobantes($desde, $hasta);
									$lista = $result['data'];
									$resumen = $result['resume'];
									$view_listado = "listado_comprobantes";
								}
						}
				}

        $data = [
            'desde'     => $desde,
            'hasta'     => $hasta,
            'lista'     => $lista,
            'resumen'   => $resumen,
            'listado'   => $listado,
            'view_listado'  => $view_listado
        ];

        if(empty($view_listado)){
            redirect('Admin-Pat/procesos');
        }

				$this->load->template("Admin-Pat/admin/$view_listado", $data);
  }

	public function herramienta_listados_excel()
    {
        if(!$this->session->userdata('is_logued_in')) {
            redirect('/');
        }

        $desde          =   $this->input->get('desde',     TRUE);
        $hasta          =   $this->input->get('hasta',     TRUE);
        $listado        =   $this->input->get('listado',   TRUE);


        $lista = [];
        $resumen = [];

        if($desde != null && $hasta != null && $listado != null) {
            if(is_valid_date($desde) && is_valid_date($hasta)) {
								$result = $this->admin_pat_model->get_listado_comprobantes($desde, $hasta);
								$lista = $result['data'];
								$resumen = $result['resume'];
        }


        if($lista === NULL) {
            return 0;
        }

        $data = [
            'lista'     => $lista,
            'resumen'   => $resumen
        ];

        $desde = str_replace("/", "", $desde);
        $hasta = str_replace("/", "", $hasta);


        header("Content-type: text/xls");
        header("Content-Disposition: attachment; filename=Listado_{$listado}_{$desde}_{$hasta}.xls");
        header("Pragma: no-cache");
        header("Expires: 0");
        echo "\xEF\xBB\xBF"; //UTF-8 BOM

        $this->load->view("Admin-Pat/admin/{$listado}_excel", $data);
    }
	}

	public function logout_ci(){
		$this->session->sess_destroy();
		redirect(base_url('Admin_Pat/login'));
	}

	public function cerrar(){
		$this->session->unset_userdata('is_logued_in');
		redirect('/');
	}
}
