<table>
	<thead>
		<th>Fecha Pago</th>
		<th>Rol</th>
		<th>Cuota</th>
		<th>Folio</th>
		<th>RUT</th>
		<th>Nombre</th>
		<th>Dirección</th>
		<th>Valor C.</th>
		<th>Interes</th>
		<th>Multa</th>
		<th>Total</th>
		<th>Fecha Cont.</th>
		<th>Cod. Autor</th>
		<th>Nro Tarjeta</th>
		<th>ID</th>
		<th>Tipo Pago</th>
	</thead>

  <tbody>
    <?php if ($lista != null): ?>
		<?php foreach ($lista as $item): ?>
      <tr>
        <td><?= formatDate($item['create_date'])?></td>
        <td><?= $item['rol']?></td>
        <td><?= $item['cuota']?></td>
        <td style="mso-number-format:'0'"><?= $item['folio']?></td>
        <td><?= $item['rut']?></td>
        <td><?= $item['nombre']?></td>
        <td><?= $item['direccion']?></td>
        <td><?= format_pesos($item['valor_cuota'])?></td>
        <td><?= format_pesos($item['interes_pagado'])?></td>
        <td><?= format_pesos($item['multa_pagado'])?></td>
        <td><?= format_pesos($item['total_cuota'])?></td>
        <td><?= formatDate($item['create_date'])?></td>
        <td><?= $item['TBK_CODIGO_AUTORIZACION']?></td>
        <td><?= $item['TBK_FINAL_NUMERO_TARJETA']?></td>
        <td><?= $item['pago_id']?></td>
        <td><?= $item['TBK_TIPO_TRANSACCION']?></td>
      </tr>
    <?php endforeach; ?>
    <tr>
      <td><strong>Pagos Realizados: <?= $resumen['pagos'] ?></strong></td>
      <td><strong>Total Recaudado:  <?= format_pesos($resumen['total_cancelado']) ?></strong></td>
    </tr>
    <?php endif; ?>
  </tbody>
</table>
