<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="col-md-9">
    <div class="panel panel-default">
        <div class="panel-heading text-center">
            <h3 class="panel-title">
                <span class="glyphicon glyphicon-stats"></span>
                <strong>Listados</strong>
            </h3>
        </div>
        <div class="panel-body">

            <form class="form-horizontal" action="<?= base_url('Admin_Pat/Admin_Pat_listados_fin') ?>" method="post" target="_blank" data-toggle="validator">
                <div class="row">
                    <div class="col-md-6">

                        <div class="row">
                            <div class="col-md-2">
                                <label class="control-label">Desde: </label>
                            </div>
                            <div class="col-md-10">

                                  <!--<div class="input-group date" data-provide="datepicker">
                                    <input type="text" class="form-control"  required><span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                  </div>-->
                                  <div class="input-group date"> <!-- Date input -->
                                    <input class="form-control" id="txtdesde" name="desde" placeholder="Seleccione un fecha" type="text"/><span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                  </div>
                            </div>
                        </div>
                        <br/>
                        <div class="row">
                            <div class="col-md-2">
                                <label class="control-label">Hasta: </label>
                            </div>
                            <div class="col-md-10">
                                <div class="input-group date">
                                    <input id="txtHasta" name="hasta"  type="text" class="form-control" placeholder="Seleccione un fecha" pattern="[0-9]{4}\/[0-9]{2}\/[0-9]{2}" required><span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                </div>

                            </div>
                        </div>

                    </div>

                    <div class="col-md-6 radio-admin">

                        <div>
                            <input type="radio" value="1" name="listado" required/><span> Listado De Comprobantes Pagados</span>
                        </div>
                    </div>
                </div>
                <br />
                <div class="row">
                    <div class="col-md-2 col-md-offset-1">
                        <button type="submit" class="btn btn-primary btn-block">
                            <span class="glyphicon glyphicon-search"></span>
                            Buscar
                        </button>
                    </div>
                </div>

            </form>

        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
      var date_input=$('input[type="text"]'); //our date input has the name "date"
      var options={
        format: "yyyy/mm/dd",
        language: "es",
        orientation: "bottom left",
        autoclose: true,
        todayHighlight: true
      };
      date_input.datepicker(options);
    })
</script>
