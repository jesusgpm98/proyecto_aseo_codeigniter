<br />
<br />

//<script>
//$(document).ready(function() {
//    $('#myTable').DataTable();
//} );
//</script>

<div class="container-fluid">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-body">

          <div class="row">
            <div class="col-md-3 col-xs-6 text-center">
              <div style="text-decoration: underline; padding-bottom: 4px;">
                <strong>Rangos de Fechas</strong>
              </div>
              <div>
                Desde:  <strong><?= $desde ?></strong>
              </div>
              <div>
                Hasta:  <strong><?= $hasta ?></strong>
              </div>
            </div>

            <?php if ($resumen != NULL): ?>

              <div class="col-md-3 col-xs-6 text-center">
                <div style="text-decoration: underline; padding-bottom: 4px;">
                  <strong>Resumen</strong>
                </div>
                <div>
                  Pagos Realizados: <strong><?= $resumen['pagos'] ?></strong>
                </div>
                <div>
                  Total Recaudado: <strong><?= format_pesos($resumen['total_cancelado']) ?></strong>
                </div>
              </div>

              <div class="col-md-3 text-center">
                <a target="_blank" href="<?= base_url("Admin_Pat/herramienta_listados_excel?listado=$view_listado&desde=$desde&hasta=$hasta") ?>" class="btn btn-success">
                  <img src="<?= base_url('/resources/img/excel.png') ?>" alt="" width="24" height="24">
                  <strong>Exportar a Excel</strong>
                </a>
              </div>
            <?php endif; ?>
          </div>

        </div>
      </div>
    </div>
  </div>

  <?php if($lista!= null) :?>
    <form method="post" action="<?= base_url("Admin_Pat/Admin_Pat_listados")?>">
      <input type="hidden" name="desde" value="<?= $desde?>"/>
      <input type="hidden" name="hasta" value="<?= $hasta?>"/>
      <input type="hidden" name="listado" value="<?= $listado?>"/>
    </form>
  <?php endif;?>

  <div class="row">
    <div class="col-md-12">
      <table class="table table-striped  table-hover table-border table-listados" id="myTable" style="font-size: 12px">

          <thead class="thead-dark">
            <th>N°</th>
            <th>Fecha Pago</th>
            <th>Rol</th>
            <th>Cuota</th>
            <th>Folio</th>
            <th>Rut</th>
            <th>Nombre</th>
            <th>Direccion</th>
            <th>Valor C</th>
            <th>Int.</th>
            <th>Mult.</th>
            <th>total</th>
            <th>Fec. Cont</th>
            <!-- <th>Monto</th> -->
            <th>Cod Autor.</th>
            <th>Nro Tarj.</th>
            <th>Id</th>
            <th>Tipo Pago</th>
          </thead>

        <?php //var_dump($lista);?>
        <?php if ($lista != null): ?>
          <tbody>
            <?php foreach ($lista as $item): ?>
              <tr>
                <td><?= $item['numero']?></td>
                <td><?= formatDate($item['create_date'])?></td>
                <td><?= $item['rol']?></td>
                <td><?= $item['cuota']?></td>
                <td><?= $item['folio']?></td>
                <td><?= $item['rut']?></td>
                <td><?= $item['nombre']?></td>
                <td><?= $item['direccion']?></td>
                <td><?= format_pesos($item['valor_cuota'])?></td>
                <td><?= format_pesos($item['interes_pagado'])?></td>
                <td><?= format_pesos($item['multa_pagado'])?></td>
                <td><?= format_pesos($item['total_cuota'])?></td>
                <td><?= formatDate($item['create_date'])?></td>
                <!-- <td><?= format_pesos($item['total_cancelado'])?></td> -->
                <td><?= $item['TBK_CODIGO_AUTORIZACION']?></td>
                <td><?= $item['TBK_FINAL_NUMERO_TARJETA']?></td>
                <td><?= $item['pago_id']?></td>
                <td><?= $item['TBK_TIPO_TRANSACCION']?></td>
                <!--<td><?= $item['create_date']?></td>-->
              </tr>
            <?php endforeach; ?>
          </tbody>

        <?php else: ?>
          <tr>
            <td colspan="22">
              <div class="alert alert-warning">
                <h5>
                  <span class="glyphicon glyphicon-alert"></span>
                  No se han encontrado registros, en los rangos de fechas indicados
                </h5>
              </div>
            </td>
          </tr>
        <?php endif; ?>
      </table>
    </div>
  </div>
</div>
