<?php defined('BASEPATH') OR exit('No direct script access allowed');?>

<div class="container">
    <div class="row">
        <div class="col-sm-6 col-md-4 col-md-offset-4">
            <div class="account-wall text-center">
              <h1 class="text-center login-title">Inicie sesión para continuar.</h1>
                <!--<img class="profile-img" src="https://lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=120"
                    alt="">-->
                <!--<h1><span class="glyphicon glyphicon-user "></span>    </h1>-->
                <form class="form-signin" data-toggle="validator" method="post" action="<?php echo base_url('Admin_Pat/index');?>">
                <div class="form-group">
                  <input type="text" class="form-control" placeholder="Usuario" name="user" id="user" required autofocus>
                  <div class="help-block with-errors"></div>
                </div>
                <div class="form-group">
                  <input type="password" class="form-control" placeholder="Contraseña" name="pass" id="pass" required>
                  <div class="help-block with-errors"></div>
                </div>
                <?php if($error): ?>
                  <div class="alert alert-danger ">
                      <span class="glyphicon glyphicon-user"></span>
                      Los datos ingresados son <strong>incorrectos</strong>.
                  </div>
                <?php endif; ?>
                <button class="btn btn-lg btn-primary btn-block" type="submit">Ingresar</button>
                <!--<label class="checkbox pull-left">
                    <input type="checkbox" value="remember-me">
                    Remember me
                </label>
                <a href="#" class="pull-right need-help">Need help? </a><span class="clearfix"></span>-->
                </form>
            </div>
            <!--<a href="#" class="text-center new-account">Create an account </a>-->
        </div>
    </div>
</div>
<br>
