<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<br>
<br>

<div class="container-fluid">

    <div class="row">

        <div class="col-md-3">

            <div class="panel panel-primary">
                <div class="panel-heading text-center">
                    <h3 class="panel-title">
                        <span class="glyphicon glyphicon-home"></span>
                        <strong>Administración</strong>
                    </h3>
                </div>
                 <div class="panel-body text-center">
                     <img src="<?= base_url('resources/img/user.png') ?>" alt="Administración" width="48" height="48">
                     <br>
                     <br>
                     Bienvenido(a) <strong><?= $nombre_admin ?></strong>
                </div>
                <ul class="list-group">
                        <a href="<?php echo base_url('Admin_Pat/Admin?section=listados');?>" class="list-group-item">
                            <span class="glyphicon glyphicon-stats"></span>
                            Listados
                        </a>


                        <!-- <a href="<?php //echo base_url('Admin_Procesos/Admin?section=valor');?>" class="list-group-item">
                            <span class="glyphicon glyphicon-usd"></span>
                            Valor Certificados
                        </a>


                        <a href="<?php //echo base_url('Admin_Procesos/Admin?section=firmante');?>" class="list-group-item">
                            <span class="glyphicon glyphicon-transfer"></span>
                            Cambio de Firmante
                        </a>


                        <a href="#" class="list-group-item">
                            <span class="glyphicon glyphicon-check"></span>
                            Desmarcar Multas
                        </a>-


                        <a href="#" class="list-group-item">
                            <span class="glyphicon glyphicon-user"></span>
                            Agregar Usuario
                        </a>


                        <a href="#" class="list-group-item">
                           <span class="glyphicon glyphicon-wrench"></span>
                           Mantención Portal
                        </a>-->

                    <a href="<?= base_url('/Admin_Pat/Cerrar')?>" class="list-group-item">
                        <span class="glyphicon glyphicon-remove"></span>
                        Cerrar Sesión
                    </a>
                </ul>
            </div>
        </div>
        <?= $contenido ?>
    </div>
</div>
