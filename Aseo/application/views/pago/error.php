<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<div class="container">
  <div class="row">
    <div class="col-md-8 col-md-offset-2">
      <div class="alert alert-warning" role="alert">
        Estimado Contribuyente,<br>
        Ocurrio un problema en el sistema por lo que lo redireccionaremos a la página anterior.
      </div>
      <script type="text/javascript">
        /*window.location.href='javascript:history.back(-1);'*/
        setTimeout('history.back(-1)', 2000)
      </script>
    </div>
  </div>
</div>
