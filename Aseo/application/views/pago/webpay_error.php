<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>


<div class="container">
    <?php if($pago_id != NULL): ?>
        <div class="row">
            <div class="col-md-8 col-md-offset-2 ">
                <div class="page-header">
                    <h3><span class="glyphicon glyphicon-remove-sign" style="color:#c83a2a;" aria-hidden="true"></span> Transacción Rechazada</h3>
                </div>
                <br />

                <div class="alert alert-danger">
                        Transacción N° <strong><?= $pago_id ?></strong> rechazada, por favor vuelva a intentarlo.
                </div>

                <br />
                Las posibles causas de este rechazo son:
                <ul>
                    <li>Error en el ingreso de los datos de su tarjeta de crédito o débito (fecha y/o código de seguridad).</li>
                    <li>Su tarjeta de crédito o débito no cuenta con el cupo necesario para cancelar la compra.</li>
                    <li>Tarjeta aún no habilitada en el sistema financiero.</li>
                </ul>
            </div>

        </div>
        <br>
    <?php else: ?>
        <?php if($reload != NULL): ?>
            <div class="row">
                <div class="col-md-8 col-md-offset-2 ">
                    <div class="alert alert-info">
                        <h2 class="text-center">
                            <strong>Información</strong>
                        </h2>
                        <br>
                        Estimado su transacción ya fue procesada, el comprobante de pago fue enviado a su <strong>correo electrónico</strong>, en caso contrario comuníquese con la Municipalidad.
                        <br>
                        <br>
                    </div>

                </div>
            </div>

        <?php else: ?>

         <?php if($anulado != NULL): ?>
            <div class="row">
                <div class="col-md-8 col-md-offset-2 ">
                    <div class="alert alert-warning">
                        <h2 class="text-center">
                            <strong>Información</strong>
                        </h2>
                        <br>
                        Estimado su transacción fue <strong>anulada</strong>.
                        <br>
                        <br>
                    </div>

                </div>
            </div>
        <?php else: ?>

            <div class="row">
                <div class="col-md-8 col-md-offset-2 ">
                    <div class="page-header">
                        <h3><span class="glyphicon glyphicon-remove-sign" style="color:#c83a2a;" aria-hidden="true"></span> Transacción Rechazada</h3>
                    </div>
                    <br />

                    <div class="alert alert-danger">
                            Transacción rechazada, por favor vuelva a intentarlo.
                    </div>

                    <br />
                    Las posibles causas de este rechazo son:
                    <ul>
                        <li>Error en el ingreso de los datos de su tarjeta de crédito o débito (fecha y/o código de seguridad).</li>
                        <li>Su tarjeta de crédito o débito no cuenta con el cupo necesario para cancelar la compra.</li>
                        <li>Tarjeta aún no habilitada en el sistema financiero.</li>
                    </ul>
                </div>
            </div>
        <?php endif; ?>

        <?php endif; ?>
    <?php endif; ?>



    <div class="row col-md-12 text-center">
        <a href="<?= base_url() ?>" class="btn btn-primary">Volver al Inicio</a>
    </div>
</div>
