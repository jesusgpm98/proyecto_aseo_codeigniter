<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="page-header">
        <h3>Confirmación de Pago</h3>
      </div>
    </div>
    <div class="col-md-8 col-md-offset-2">
    	<div class="table-responsive">
    		<table class="table table-striped table table-bordered">
    			<thead class="thead-default">
    				<tr>
    					<th>Usted está pagando:</th><td>Derechos de Aseo</td>
    				</tr>
    				<tr>
    					<th>Usted está pagando en:</th><td>Pesos Chilenos, Santiago, Chile.</td>
    				</tr>
    				<tr>
    					<th>Url comercio:</th><td><a href="<?php echo base_url();?>" target="_blank">Url Comercio</a></td>
    				</tr>
    				<tr>
    					<th>Fecha y Hora de pago:</th><td><?php echo date('d-m-Y h:i')."Hrs";?></td>
    				</tr>
    			</thead>
    		</table>
    	</div>
    </div>
    <div class="col-md-12">
      <div class="page-header">
    		<h3>Resumen de Pago</h3>
    	</div>
    </div>
    <div class="col-md-8 col-md-offset-2">
      <div class="table-responsive">
        <table class="table table-bordered">
          <thead class="thead-default">
            <tr>
              <th>Origen del Pago</th>
              <th>Detalle</th>
              <th>Monto</th>
            </tr>
            <?php
              foreach($datos as $row):
                $datos = explode('_',$row);
            ?>
              <tr>
                <td>Pago Derechos de Aseo</td>
                <td>Rol <?php echo $datos[3];?>, Año <?php echo $datos[0].' cuota '.$datos[2];?></td>
                <td><?php echo formatPesos($datos[1]);?></td>
              </tr>
            <?php endforeach;?>
            <tr>
					   <td colspan="2" align="right">TOTAL A PAGAR</td>
					   <td><?php echo formatPesos($txtTotal);?></td>
				    </tr>
          </thead>
        </table>
      </div>
    </div>
    <div class="col-md-8 col-md-offset-2">
      <div class="col-md-6">
        <button class="btn btn-default" onclick="window.history.back();">Volver</button>
      </div>
      <div class="col-md-6 text-right">
        <form action="<?= $webpay->url ?>" method="post">
          <input type="hidden" name="token_ws" value="<?= $webpay->token ?>">
          <input type="submit" class="btn btn-success" name="pagar" value="Pagar">
        </form>
      </div>
    </div>
  </div>
</div>
