<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<script>
        $( document ).ready(function() {
          $('#ModalAtencion').modal('show');
      })
    </script>

<div class="container">

  <div id="ModalAtencion" data-backdrop="static" data-keyboard="false" class="modal fade">
      <div class="modal-dialog">
          <div class="modal-content">

              <div class="modal-header">

                  <center><h4>¡ATENCIÓN!</h4></center>
              </div>
              <div class="modal-body">
                <strong>Estimado Contribuyente, </strong><br /><br>

                  Para la visualización de cada uno de sus comprobantes de pago dirígase a la parte superior derecha
                  <strong>"Re-Imprime tu Pago"</strong>.<br><br>

                  Aclarado esto, aceptar y finalizar.
              </div>
              <!-- dialog buttons -->
              <div class="modal-footer">
                  <input type="button"  class="btn btn-success" name="btnAceptarModal" id="btnAceptarModal" value="Aceptar" data-dismiss="modal"  >

              </div>
          </div>
      </div>
    </div>


    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-info">
                El comprobante de pago fue enviado a su <strong>correo electrónico</strong>, en caso contrario comuníquese con la Municipalidad.
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-xs-12 col-md-12 ">
            <div class="page-header">
                <h3>
                    <span class="glyphicon glyphicon-ok-circle text-success" aria-hidden="true"></span>
                    Transacción Finalizada
                </h3>
            </div>
        </div>
        <div class="col-md-8 col-md-offset-2">
            <div class="table-responsive">
                <table class="table table-striped table table-bordered">
                    <tr>
                        <th>Código Transacción</th><td><?= $webpay->buyOrder ?></td>
                    </tr>
                    <tr>
                        <th>Fecha Pago</th><td><?= $webpay->transactionDate ?></td>
                    </tr>
                    <tr>
                        <th>Monto Pagado</th><td><?= formatPesos($webpay->detailOutput->amount)  ?></td>
                    </tr>
                    <tr>
                        <th>Medio de Pago</th><td>WEBPAY</td>
                    </tr>
                    <tr>
                        <th>Modalidad de Pago</th>
                        <td>
                            <?= format_payment_code($webpay->detailOutput->paymentTypeCode) ?>
                        </td>
                    </tr>
                    <tr>
                        <th>Cuotas</th><td><?= $webpay->detailOutput->sharesNumber ?></td>
                    </tr>
                    <tr>
                        <th>Código Autorización</th><td><?= $webpay->detailOutput->authorizationCode ?></td>
                    </tr>
                    <tr>
                      <th>Comprobante Pago</th>
                      <!-- <td>
                      <form method="post" action="<?= base_url('Datos_pago/FirmaDoc')?>">
                        <input type="hidden" name="pagoId" value="<?= $webpay->buyOrder ?>">
                        <input type="submit" name="btnDescargar" value="Descargar" class="btn btn-success" formtarget="_blank">
                      </form>
                      </td> -->
                      <?php if($ruta_pdf != ""): ?>
                        <td><a href="<?= $ruta_pdf ?>" target="_blank" class="btn btn-primary">Descargar</a></td>
                      <?php else:?>
                      <?php endif;?>
                    </tr>
                </table>
            </div>
            <div class="text-right">
                <a type="button" class="btn btn-primary" href="<?= base_url() ?>">Terminar </a>
            </div>
        </div>
    </div>
</div>
