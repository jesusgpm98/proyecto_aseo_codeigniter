<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="page-header">
        <h1>Información de Pagos</h1>
      </div>
      <div class="col-md-8 col-md-offset-2">
        <table class="table table-hover">
          <thead>
            <th>Año</th>
            <th>Cuota</th>
            <th>Rol</th>
            <th>Acción</th>
          </thead>
          <tbody>
            <?php if(!$pago){ ?>
                <div class="alert alert-warning" role="alert">
                  Estimado Contribuyente: <br>
                  No se han encontrado registros.
                </div>
              </div>
              <?php }else{ ?>
            <?php foreach($pago as $row):

              $split = explode('-', $row['rol']);
              //var_dump($row);
              $rol = $split[0];
              $rol_dv = $split[1];
              //var_dump($row['pago_id']);
              ?>
              <tr>
                <td><?php echo $row['ano'];?></td>
                <td><?php echo $row['cuota'];?></td>
                <td><?php echo $row['rol'];?></td>



                  <form class="" action="<?= base_url('Datos_pago/Firmadoc') ?>" method="post" >

                  <input type="hidden" name="pago_id" value="<?php echo $row['pago_id']; ?>">
                  <input type="hidden" name="ano" value="<?php echo $row['ano']; ?>">
                  <input type="hidden" name="cuota" value="<?php echo $row['cuota']; ?>">
                  <input type="hidden" name="rol" value="<?php echo $rol ?>">
                  <input type="hidden" name="rol_dv" value="<?php echo $rol_dv ?>">
                  <?php if($row['doc_descar']==null ){?>
                    <td><a href="<?= base_url('Datos_pago/reimpresion');?>?pago_id=<?php
                    echo $row['pago_id'];?>&ano=<?php echo $row['ano']; ?>&cuota=<?php echo $row['cuota']; ?>
                    &rol=<?php echo $rol ?>&rol_dv=<?php echo $rol_dv ?>" type="button" target="_blank" class="btn btn-success" />Descargar</a></td>
                  <?php }else{?>
                    <td><a href="<?= $row['doc_descar'];?>" type="button" target="_blank" class="btn btn-success" />Descargar</a></td>
                  <?php }?>
                </form>
                
              </tr>
            <?php endforeach;?>
            <?php }?>
          </tbody>
        </table>

      </div>

        <div class="col-md-6">
        </div>

      <div class="col-md-6">
        <div class="form-group text-right">
          <input type="button" class="btn btn-success" name="volver" onclick="window.history.back();" value="Volver">
        </div>
      </div>
    </div>
  </div>
</div>
