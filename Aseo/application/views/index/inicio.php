<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script>
$(document).ready(function(){

	$('#txtRut').Rut({
		format: false,
  	digito_verificador: '#txtDv',
  	on_error: function(){ alert('Rut incorrecto'); }
	});

	$("#content > ul").tabs();
});
</script>
<div class="container">
	<div class="row">
		<div class="col-md-8">

			<h3 align=center>Bienvenido a nuestro Portal de Derechos de Aseo</h3><br />
			<div class="panel-group" id="accordion">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">
        			<a data-toggle="collapse" data-parent="#accordion" href="#collapse1" data-target="#collapse1">
        			INFORMACIÓN</a>
      			</h4>
					</div>
					<div id="collapse1" class="panel-collapse collapse in">
      			<div class="panel-body">
							<strong>Estimado contribuyente</strong>

							<br /><br /><p>Durante los meses de Mayo, Julio, Octubre y Diciembre de cada año, corresponde cancelar
								 los Derechos de Aseo domiciliario. Encarecemos a Ud. cumplir con esta obligación que nos permite mantener
								  nuestro Servicio de Aseo, así como también la mantención y ornato de plazas y otros espacios públicos de
									la comuna. En caso de presentar problemas socioeconómicos que le impidan realizar sus pagos,
									debe asistir a la Dirección de Desarrollo Comunitario (DIDECO), para solicitar evaluación con un
									Asistente Social y posteriormente, si procede, presentar el certificado en el Departamento de Rentas.
									Este trámite debe realizarse antes del 15 de enero de cada año. </p>
								<br />
								<p>Adicionalmente, puede buscar un Rol por Rut en el <a href="https://zeus.sii.cl/avalu_cgi/br/brc110.sh?" target="_blank">Servicio de Impuestos Internos (SII).</a></p>
								<p>Sólo se consideran los roles que se encuentran actualizados en sus pagos. Consultas al fono: <strong>8218626</strong>
									e-mail <strong>derechosdeaseo@paine.cl</strong></p>
      			</div>
    			</div>
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="row">
				<h4>Por favor ingrese el Rol (Manzana y Predio) de la Propiedad que le corresponda</h4>
				<form class="form" action="<?php echo base_url('Datos_contribuyente/Informacion');?>" method="post" role='form' name='formulario' id='formulario'	data-toggle=validator>
					<div class="col-md-6">
						<div class="form-group">
							<label for="">Rol</label>
							<input type="number" class="form-control" name="txtRol" value="" placeholder="123" required>
							<div class="help-block with-errors"></div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label for="" style="color:white">Rol</label>
							<input type="number" class="form-control" name="txtRolDv" value="" placeholder="1" required>
							<div class="help-block with-errors"></div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label for="">Correo</label>
							<input type="email" class="form-control" name="txtCorreo" value="" data-error="Formato de correo invalido" required>
							<div class="help-block with-errors"></div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<div class="form-group">
								<button type="submit" class="btn btn-warning col-xs-12 ">Derecho de Aseo</button>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
