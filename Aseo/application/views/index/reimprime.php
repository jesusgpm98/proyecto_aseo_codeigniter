<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="page-header">
        <h1>Reimprima su comprobante de pago</h1>
      </div>
      <div class="col-md-8 col-md-offset-2">
        <div class="alert alert-info" role="alert">
          <strong>Atención</strong><br>
          Ingrese los datos a continuación para obtener su Comprobante<br>
          y posteriormente presione el botón Buscar.
        </div>
        <div class="col-md-12">
          <div class="col-md-8 col-md-offset-2">
            <form class="" action="<?php echo base_url('Inicio/reimpresion'); ?>" method="post" name="formulario" id="formulario" data-toggle="validator">
              <div class="col-md-12">
                <div class="row">
                  <div class="col-md-6">
                    <label>Rol</label>
                    <div class="form-group">
                      <input type="text" name="Txtrol" value="" class="form-control" placeholder="1234" required pattern="^([0-9]){6-7}$">
                      <div class="help-block with-errors"></div>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <label>Rol</label>
                    <div class="form-group">
                      <input type="text" name="txtRolDv" value="" class="form-control" placeholder="56" required pattern="^([0-9]){6-7}$">
                    </div>
                  </div>

                  <div class="col-md-6">

                  </div>
                  <div class="col-md-6">
                    <div class="form-group text-right">
                      <input type="submit" name="" value="Buscar" class="btn btn-success">
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
