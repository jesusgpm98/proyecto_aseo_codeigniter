
<!DOCTYPE html>
<html lang="es">
    <head>
        <style>
            body {
                font-family: 'Arial';
                padding: 0;
                margin: 0;

            }
            .table td, .table th {
                border: 1px solid #ddd;
                text-align: left;
                padding: 15px;
            }

            .table {
                border-collapse: collapse;
                min-width: 80%;
            }

            .table th {
                text-align: left;
            }

            .table td {
                padding: 4px 16px;
            }

            .table tr:nth-child(even) {background-color: #f2f2f2}

        </style>
    </head>
    <body>

        <table style="width: 100%;height:100;">

            <tr style="background-color: #00498E;">
                <td style="padding: 20px;">
                    <img alt="Paine" src="<?php echo base_url("resources/img/logo_municipal_paine.png"); ?>" width="200" height="80">
                </td>
            </tr>
            <tr>
                <td style="text-align: center;">
                    <br /><br />
                    <h2 style="font-weight: 100;">COMPROBANTE DE PAGO</h2>
                </td>
            </tr>

            <tr>
                <td style="padding-left: 10%; padding-bottom: 50px; padding-right: 10%;">
                    <br />
                    Estimado(a) <?= $info['nombre'] ?>:

                    <br><br>
                    Junto con saludarlo(a), le informamos que acaba de realizar exitosamente el pago en linea de Derechos de Aseo
                    rol <strong><?= $info['rol'] ?>-<?= $info['rol_dv']?></strong>, a continuación los detalles del pago:



                </td>
            </tr>

            <tr>
                <td>
                    <table class="table" align="center">
                        <tr>
                            <th>Código Transacción</th>
                            <td><?= $wp['TBK_ORDEN_COMPRA'] ?></td>
                        </tr>
                        <tr>
                            <th>Fecha Pago</th>
                            <td><?= formatDate($wp['create_date']) ?></td>
                        </tr>
                        <tr>
                            <th>Monto</th>
                            <td><?= formatPesos($wp['total_cancelado']) ?></td>
                        </tr>
                        <tr>
                            <th>Medio de Pago</th>
                            <td>WEBPAY</td>
                        </tr>
                        <tr>
                            <th>Modalidad de Pago</th>
                            <td>
                                <?= format_payment_code($wp['TBK_TIPO_PAGO']) ?>
                            </td>
                        </tr>
                        <tr>
                            <th>Cuotas</th>
                            <td><?= $wp['TBK_NUMERO_CUOTAS'] ?></td>
                        </tr>
                        <tr>
                            <th>Código Autorización</th>
                            <td><?= $wp['TBK_CODIGO_AUTORIZACION'] ?></td>
                        </tr>
                        <tr>
                          <th>Certificado electrónico</th>
                          <td><a href="<?= $info['doc_descar']?>">Descargar</a></td>
                        </tr>
                    </table>
                </td>

            </tr>
            <tr>
                <td>
                     <br />
                    <br />
                </td>
            </tr>
            <tr style="background-color: #00498E;">
                <td style="color:white; font-size: 8pt; padding: 16px 4px;text-align: center;">

                    SISTEMA DESARROLLADO POR <a href="http://www.insico.cl" style="color: white;">INSICO SA.</a>
                </td>
            </tr>

        </table>

    </body>
</html>
