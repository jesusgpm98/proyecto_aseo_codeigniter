<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <link rel="icon" href="<?php echo base_url("resources/img/paine_tittle.png");?>">
  <link href="<?php echo base_url("resources/css/bootstrap.css");?>" rel="stylesheet">
  <link href="<?php echo base_url("resources/css/site.css");?>" rel="stylesheet">
  <link href="<?php echo base_url("resources/css/loading.css");?>" rel="stylesheet">
  <link href="<?php echo base_url("resources/css/loading-btn.css");?>" rel="stylesheet">
  <link href="<?php echo base_url("resources/css/bootstrap-datepicker3.css");?>" rel="stylesheet">
  <link href="<?php echo base_url("resources/css/glyphicons.css");?>" rel="stylesheet">

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script	language="JavaScript" src="<?php echo base_url("resources/js/bootstrap.js");?>"></script>
  <script	language="JavaScript" src="<?php echo base_url("resources/js/jquery-ui.min.js");?>"></script>
  <script	language="JavaScript" src="<?php echo base_url("resources/js/bootstrap-filestyle.min.js");?>"></script>
  <script	language="JavaScript" src="<?php echo base_url("resources/js/validator.js");?>"></script>
  <script	language="JavaScript" src="<?php echo base_url("resources/js/bootbox.min.js");?>"></script>
  <script	language="JavaScript" src="<?php echo base_url("resources/js/jquery.Rut.js");?>"></script>
  <script	language="JavaScript" src="<?php echo base_url("resources/js/map.js");?>"></script>
  <script	language="JavaScript" src="<?php echo base_url("resources/js/jquery.dataTables.min.js");?>"></script>
  <script	language="JavaScript" src="<?php echo base_url("resources/js/dataTables.bootstrap.min.js");?>"></script>
  <script	language="JavaScript" src="<?php echo base_url("resources/js/bootstrap-datepicker.js");?>"></script>
  <script	language="JavaScript" src="<?php echo base_url("resources/js/bootstrap-datepicker.es.min.js");?>"></script>
  <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyByFEIA4BFenHH0iRmQGkSFPDeVOMjPgFU&libraries=adsense&sensor=true&language=es"></script>

  <title>Pago de Aseo</title>

  </head>
  <body>
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation" style="background-color:#00498E;">
      <div class="container-fluid">
        <div class="navbar-header">
          <a class="navbar-brand" href="<?php echo base_url();?>">
          <img alt="Paine" src="<?php echo base_url("resources/img/logo_municipal_paine.png");?>"  height="80">
          </a>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav navbar-right">
            <?php if(!$this->session->userdata('is_logued_in')){ ?>
              <li><a href="<?php echo base_url('Admin_Pat/login');?>" style="color:white;"><span class="glyphicon glyphicon-user"></span> Inicio Sesión</a></li>
              <li><a href="<?= base_url('Inicio/reimprimir') ?>"><span class="glyphicon glyphicon-print"></span> Re-Imprime tu Pago</a></li>
            <?php }else{ ?>
              <li><a href="<?php echo base_url('Admin_Pat/Admin')?>"><span class="glyphicon glyphicon-user"></span> <?php echo $this->session->userdata('nombre').' '.$this->session->userdata('app'); ?></a></li>
              <li><a href="<?php echo base_url('Admin_Pat/logout_ci');?>"><span class="glyphicon glyphicon-remove-sign"></span> Cerrar Sesión</a></li>
            <?php } ?>
          </ul>
        </div>
      </div>
    </nav>
