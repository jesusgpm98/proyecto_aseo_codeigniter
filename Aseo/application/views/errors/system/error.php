<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="container">
  <div class="row">
    <div class="col-md-8 col-md-offset-2">
      <div class="alert alert-danger">
        <strong><?php ?>Ha ocurrido un error al seleccionar el pago.</strong>
      </div>
      <div>
        <input type="button" name="btnVolver" value="Volver" class="btn btn-primary" onclick="location.href='<?php echo base_url('Inicio/index')?>';">
      </div>
    </div>
  </div>
</div>
