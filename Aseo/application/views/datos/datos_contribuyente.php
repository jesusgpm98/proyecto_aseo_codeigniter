<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<script>
function sumar(check){
  var form = document.formulario;
	var elems = form.elements;
  var suma = 0;
  var i=0;
  document.formulario.otro_total.value = 0;
  //document.getElementById("txtTotal").disabled = TRUE;

  for (var i = 0 ; i < elems.length ; i++) {
				if (elems[i].type == 'checkbox' && !elems[i].checked) {
					if (form.elements[i+1].checked || !elems[i+1].disabled) {
						elems[i+1].disabled = true;
						form.elements[i+1].checked=false;

					}
				}
			}


			for (var i = 0 ; i <= elems.length ; i++) {
				if (elems[i].type == 'checkbox' && elems[i].checked) {
          var a = elems[i].value.split('_',2);
          var subtotal = a[1];

          //alert('a ' + a);
          suma += parseInt(subtotal);
          /*Habilitamos el checkbox siguiente*/
					elems[i+1].disabled = false;
					/*Fin*/
				}

        document.getElementById("txtTotal").value = suma;
        document.formulario.otro_total.value = suma;
          //$("#txtTotal").val(suma);
          //document.getElementById("txtTotal").value = suma;

			}


  }
</script>

<script>
        $( document ).ready(function() {
          $('#ModalAtencion').modal('show');

        $('#chkCondicion').on("click", function ()
        {
            var booleano = $('#chkCondicion').is(":checked");
            if (booleano)
            {
                $("#btnAceptarModal").removeAttr("disabled");
            } else
            {
                $("#btnAceptarModal").prop("disabled", true);
            }

        });
      })
    </script>
<?php if(!$cliente):?>
  <div class="container">

    <div id="ModalAtencion" data-backdrop="static" data-keyboard="false" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">

                    <center><h4>¡ATENCIÓN!</h4></center>
                </div>
                <div class="modal-body">
                    Estimado Contribuyente, <br />
                    Antes de realizar el pago, verifique que la informacion desplegada corresponda a su domicilio.

                    Recuerde que el ingreso de sus datos deben ser los correctos, si tiene un caracter erróneo no
                    se cargarán sus datos, por ende no se generará la informacion para el pago correspondiente.<br />
                    Aclarado esto por favor aceptar y continuar.

                </div>
                <!-- dialog buttons -->
                <div class="modal-footer">
                    <label>
                        <input type="checkbox" id="chkCondicion" required="" value="False" name="chkCondicion">
                        <strong> Acepto Condiciones</strong>
                    </label>
                    <a href="<?php echo base_url("Inicio"); ?>" class="btn btn-danger  active" role="button">Salir</a>

                    <input type="button" disabled onclick="return false()" class="btn btn-success" name="btnAceptarModal" id="btnAceptarModal" value="Aceptar"   data-dismiss="modal"  >

                </div>
            </div>
        </div>
      </div>

    <div class="row">
      <div class="col-md-8 col-md-offset-2">
        <div class="alert alert-warning" role="alert">
          El Rol ingresado no se encuentra registrado en nuestra base de datos, fue ingresado erroneamente o se encuentra dentro de los Derechos de Aseo que no pueden ser pagados por este portal<br> <br>

        </div>
      </div>
      <div class="col-md-12">
        <div class="col-md-6">

        </div>
        <div class="col-md-6 text-right">
          <button class="btn btn-default" onclick="window.history.back();">Volver</button>
        </div>
      </div>
    </div>
  </div>
<?php else:?>
  <div class="container">

    <div id="ModalAtencion" data-backdrop="static" data-keyboard="false" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <center><h4>¡ATENCIÓN!</h4></center>
                </div>
                <div class="modal-body">
                  Estimado Contribuyente, <br />
                  Antes de realizar el pago, verifique que la informacion desplegada corresponda a su domicilio.

                  Recuerde que el ingreso de sus datos deben ser los correctos, si tiene un caracter erróneo no
                  se cargarán sus datos, por ende no se generará la informacion para el pago correspondiente.<br />
                  Aclarado esto por favor aceptar y continuar.
                </div>
                <!-- dialog buttons -->
                <div class="modal-footer">
                    <label>
                        <input type="checkbox" id="chkCondicion" required="" value="False" name="chkCondicion">
                        <strong> Acepto Condiciones</strong>
                    </label>
                    <a href="<?php echo base_url("Inicio"); ?>" class="btn btn-danger  active" role="button">Salir</a>
                    <input type="button" disabled onclick="return false()" class="btn btn-success" name="btnAceptarModal" id="btnAceptarModal" value="Aceptar"   data-dismiss="modal">
                </div>
            </div>
        </div>
      </div>

    <div class="row">
      <form class="form-inline" action="<?php echo base_url('Datos_pago/Confirmacion');?>" method="post" role="form" name="formulario" id="formulario">
          <input type="hidden" name="otro_total" id="otro_total" >
          <div class="col-md-9">
    				<?php foreach($cliente as $row): ?>
    				<h1><?php echo $row['nombre'];?></h1>
    				<h4><span class="glyphicon glyphicon-user" style="color:grey;"></span><span> <?php echo $row['rut'];?> - <?php echo $row['rut_dv'];?></span></h4>
            <h4><span class="glyphicon glyphicon-map-marker" style="color:grey;"></span><span> <?php echo $row['direccion'];?></h4>
    				<h4><span class="glyphicon glyphicon-shop" style="color:grey;"></span><span> <?php echo $row['rol'];?> - <?php echo $row['rol_dv'];?></span></span></h4>
            <h4><span class="glyphicon glyphicon-envelope" style="color:grey;"></span><span> <?php echo $correo; ?></span></h4>
            <?php endforeach; ?>
    			</div>
          <div class="col-md-12 ">
    				<div class="page-header">
    				  <h3>Detalle de Pago</h3>
    				</div>
            <?php if(!$pagos){ ?>

                <div class="col-md-8 col-md-offset-2">
                  <div class="alert alert-warning" role="alert">
                    Estimado Contribuyente: <br>
                    En este momento no se encuentra con deudas.
                  </div>
                </div>
                <div class="col-md-6 text-left">
                  <div class="form-group">
                    <input type="button" name="btnVolver" value="Volver" class="btn btn-primary" onclick="location.href='<?php echo base_url('Inicio/index')?>';">
                  </div>
        				</div>
        				<div class="col-md-6 text-right">
        				</div>

              <?php }else{ ?>
            <table class="table table-bordered">
              <thead>
                <th>Selección</th>
                <th>Año</th>
                <th>Cuota</th>
                <th>Fecha Vencimiento</th>
                <th>Sub Total</th>
                <th>Interés</th>
                <th>Reajuste</th>
                <th>TOTAL</th>
              </thead>
              <tbody>
                <?php foreach($pagos as $row): ?>
                  <?php foreach($cliente as $cli): ?>
                  <tr>
                    <?php $xx = $row['valor_cuota'] + $row['interes_pagado'] + $row['multa_pagado'];?>
                    <?php //$pat = $row['valor_aseo_adic'] + $row['valor_otro_1'] + $row['valor_otro_2'] + $xx;?>
                    <?php //$total = $pat + $row['interes'] + $row['multa'];?>

                      <td><input type="checkbox" value="<?php echo $row['ano'].'_'.$xx. '_' . $row['cuota']. '_' . $cli['rol'] . '-' . $cli['rol_dv'];?>"  onclick="javascript:return sumar(this)" name="check_aseo[]"></td>

                      <!--<td><input type="checkbox" value="" /></td>-->
                      <td><?php echo $row['ano'];?></td>
                      <td><?php echo $row['cuota'];?></td>
                      <td><?php echo formatDate($row['fec_vcto']);?></td>
                      <td><?php echo formatPesos($row['valor_cuota']);?></td>
                      <td><?php echo formatPesos($row['interes_pagado']);?></td>
                      <td><?php echo formatPesos($row['multa_pagado']);?></td>
                      <td><?php echo formatPesos($xx);?></td>

                  </tr>
                <?php endforeach; ?>
                <?php endforeach; ?>
              </tbody>
            </table>
            <div class="col-md-6 text-left">
    				</div>
    				<div class="col-md-6 text-right">
    					<div class="form-group">
    						<label for="txtTotal">Total</label>
    						<input type="text" class="form-control" name="txtTotal" id="txtTotal" placeholder="Total" readonly>
                <script>
  										$("#formulario").submit(function () {
      								   if($("#txtTotal").val().length == '' || $("#txtTotal").val().length == 0) {
  										   bootbox.alert("Seleccione un Pago");
          						   return false;
      								   }
      								//return false;
  										});
  							</script>
    					</div>
              <br><br>
              <div class="form-group">
                <input type="submit" name="btnContinuar" value="Continuar" class="btn btn-success">
              </div>
    				</div>
          <?php } ?>
          </div>
      </form>
    </div>
  </div>
<?php endif; ?>
